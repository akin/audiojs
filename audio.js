'use strict';

var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
var button = document.querySelector('button');
var input = document.querySelector('input');

class AudioBuffer
{
    constructor(config)
    {
        this.context = config.context;
        this.channels = config.channels;
        this.length = config.length;

        this.rate = this.context.sampleRate;
        this.rateInverse = 1.0 / this.rate;
        this.frameCount = this.rate * this.length;

        this.buffer = this.context.createBuffer(this.channels, this.frameCount, this.rate);
    }

    create()
    {
        return new AudioBuffer({
            context: this.context,
            channels: this.channels,
            length: this.length
        });
    }
}

class AudioNode
{
    constructor(config)
    {
        this.context = config.context;
    }

    process(input)
    {
        if(this.next != undefined)
        {
            for(var i = 0 ; i < this.next.length ; ++i)
            {
                this.next[i].process(input);
            }
        }
    }

    add(node)
    {
        if(this.next == undefined)
        {
            this.next = [];
        }
        this.next.push(node);
    }
}

// Plays the sound
class SinkNode extends AudioNode
{
    constructor(config)
    {
        super(config);

        // Get an AudioBufferSourceNode.
        // This is the AudioNode to use when we want to play an AudioBuffer
        this.source = this.context.createBufferSource()
    }

    process(input)
    {
        this.source = this.context.createBufferSource()
        this.source.buffer = input.buffer;
        // connect the AudioBufferSourceNode to the
        // destination so we can hear the sound
        this.source.connect(this.context.destination);
        this.source.start();

        super.process(input);
    }
}

class SineNode extends AudioNode
{
    constructor(config)
    {
        super(config);

        if(config.speed == undefined)
        {
            config.speed = 1.0;
        }
        this.speed = config.speed;
    }

    process(input)
    {
        var data = input.create();

        for (var channel = 0; channel < input.channels; ++channel) 
        {
            var src = input.buffer.getChannelData(channel);
            var dst = data.buffer.getChannelData(channel);
            for (var i = 0; i < input.frameCount; ++i) 
            {
                var at = i * input.rateInverse;

                dst[i] = Math.sin(at * (2*Math.PI) * this.speed);
            }
        }

        super.process(data);
    }

    setSpeed(value)
    {
        this.speed = value;
    }
}

class WhiteNoiseNode extends AudioNode
{
    constructor(config)
    {
        super(config);
    }

    process(input)
    {
        var data = input.create();
        for (var channel = 0; channel < input.channels; ++channel) 
        {
            var src = input.buffer.getChannelData(channel);
            var dst = data.buffer.getChannelData(channel);
            for (var i = 0; i < input.frameCount; ++i) 
            {
                // Math.random() is in [0; 1.0]
                // audio needs to be in [-1.0; 1.0]
                dst[i] = Math.random() * 2 - 1;
            }
        }

        super.process(data);
    }
}

class AbsNode extends AudioNode
{
    constructor(config)
    {
        super(config);
    }

    process(input)
    {
        var data = input.create();
        for (var channel = 0; channel < input.channels; ++channel) 
        {
            var src = input.buffer.getChannelData(channel);
            var dst = data.buffer.getChannelData(channel);
            for (var i = 0; i < input.frameCount; ++i) 
            {
                dst[i] = Math.abs(src[i]);
            }
        }

        super.process(data);
    }
}

class FadeOutNode extends AudioNode
{
    constructor(config)
    {
        super(config);
    }

    process(input)
    {
        var data = input.create();
        for (var channel = 0; channel < input.channels; ++channel) 
        {
            var src = input.buffer.getChannelData(channel);
            var dst = data.buffer.getChannelData(channel);
            for (var i = 0; i < input.frameCount; ++i) 
            {
                dst[i] = src[i] * (1.0 - (i / input.frameCount));
            }
        }

        super.process(data);
    }
}

class AverageNode extends AudioNode
{
    constructor(config)
    {
        super(config);
        this.avg = config.averageCount;
    }

    process(input)
    {
        var data = input.create();
        for (var channel = 0; channel < input.channels; ++channel) 
        {
            var src = input.buffer.getChannelData(channel);
            var dst = data.buffer.getChannelData(channel);
            for (var i = 0; i < input.frameCount; ++i) 
            {
                var remaining = input.frameCount - i;
                var count = remaining > this.avg ? this.avg : remaining;

                var number = 0;
                for(var i2 = 0 ; i2 < count ; ++i2)
                {
                    number += src[i + i2];
                }

                dst[i] = number / count;
            }
        }

        super.process(data);
    }
}

var buffer = new AudioBuffer({
    context: audioCtx,
    channels: 2,
    length: 2.0
});

var white = new WhiteNoiseNode({
    context: audioCtx
});

var sine = new SineNode({
    context: audioCtx,
    speed: 120.0
});

var abs = new AbsNode({
    context: audioCtx
});

var average = new AverageNode({
    context: audioCtx,
    averageCount: 10
});

var fadeout = new FadeOutNode({
    context: audioCtx
});

var sink = new SinkNode({
    context: audioCtx
});
var sink2 = new SinkNode({
    context: audioCtx
});

var root = new AudioNode({
    context: audioCtx
});

root.add(sine);
sine.add(sink);
sine.add(abs);
//abs.add(sink2);
/*
white.add(abs);
abs.add(average);
average.add(sink);
fadeout.add(sink);
/**/

button.onclick = function() 
{
    sine.setSpeed(input.value);
    root.process(buffer);
}